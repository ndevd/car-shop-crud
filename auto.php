<?php

class Auto {
    
    //Atributos o propiedades
    private $marca;
    private $modelo;
    private $precio;

    //Constructor
    public function __construct($mar, $mod, $pre){
        $this->marca  = $mar;
        $this->modelo = $mod;
        $this->precio = $pre;
    }

    //Metodos Setters
    public function setMarca($ma){
        $this->marca = $ma; 
    }
    public function setModelo($mo){
        $this->modelo = $mo; 
    }
    public function setPrecio($pr){
        $this->precio = $pr; 
    }

    //Metodos Getters
    public function getMarca(){
        return $this->marca;
    }
    public function getModelo(){
        return $this->modelo;
    }
    public function getPrecio(){
        return $this->precio;
    }

    //Metodos
    public function __toString(){
        return 'Marca: '.$this->marca.', Modelo: '.$this->modelo. ', Precio: '.$this->precio.'.';
    }

}