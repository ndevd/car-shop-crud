<?php
class Cliente {

    //Atributos o propiedades
    private $nombre;
    private $apellido;
    private $telefono;

    //Constructor
    public function __construct($nom, $ape, $tel){
        $this->nombre  = $nom;
        $this->apellido = $ape;
        $this->telefono = $tel;
    }

    //Metodos Setters
    public function setNombre($n){
        $this->nombre = $n; 
    }
    public function setApellido($a){
        $this->apellido = $a; 
    }
    public function setTelefono($t){
        $this->telefono = $t; 
    }

    //Metodos Getters
    public function getNombre(){
        return $this->nombre;
    }
    public function getApellido(){
        return $this->apellido;
    }
    public function getTelefono(){
        return $this->telefono;
    }

    //Metodos
    public function __toString(){
        return 'Nombre: '.$this->nombre.', Apellido: '.$this->apellido. ', Telefono: '.$this->telefono.'.';
    }

} 