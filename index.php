<?php
require_once 'auto.php';
require_once 'cliente.php';

//Istancias de la clase Auto - marca, modelo, precio
$auto1 = new Auto('Fiat', 'Uno', 'U$S 3800');
$auto2 = new Auto('Volkswagen', 'Fusca', '$ 35000');

echo "<center>";

echo "<br> <b>Listado de autos</b> <br>";
echo $auto1;
echo "<br>";
echo $auto2;

echo "<br>";

//Instancias de la clase Cliente - nombre, apellido, telefono
$cliente1 = new Cliente('Nestor', 'Benitez', 25846971);
$cliente2 = new Cliente('Andrea', 'Usher', 35698784);
$cliente3 = new Cliente('German', 'Fernandez', 62548731);


echo "<br> <b>Listado de clientes</b> <br>";
echo $cliente1;
echo "<br>";
echo $cliente2;
echo "<br>";
echo $cliente3;

echo "<br>";

//Mostrar algunos datos de los objetos creados
echo "<br> <b>Datos de autos (algunos)</b> <br>";
echo "El auto 1 cuesta ".$auto1->getPrecio().".";
echo "<br>";
echo "La marca del auto 2 es ".$auto2->getMarca() .".";

echo "<br>";

echo "<br> <b>Datos de clientes(algunos)</b> <br>";
echo "El cliente 2 se llama ".$cliente2->getNombre() . ".";
echo "<br>";
echo "El numero de telefono del cliente 3 es ".$cliente3->getTelefono() . ".";

echo "</center>";